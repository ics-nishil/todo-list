import React, { useState ,useEffect } from 'react';
import './App.css'
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import Table from 'react-bootstrap/Table';

const getLocalItem =()=>{
    let list = localStorage.getItem('lists')
    //console.log(list);
    if(list){
        return JSON.parse(list)
    }else{
        return [];
    }
}
function App() {
    const [text, setText] = useState("")
    const [task, setTask] = useState(getLocalItem())
    
    const changeText = (e) =>{
        setText(e.target.value)
    }

    const submitHandler = (e) => {
        window.alert("submited");
        setTask([...task, text])
        setText("")
    }

    const removeTask =(a)=>{
        const finalData = task.filter((ele,index)=>{
          //console.log(ele)  
          return index !== a;
        })

        setTask(finalData)
    }

    const updateTask=(a)=>{

      const newEditItems = task[a]
      
      let tmp1 = getLocalItem()
      tmp1[a]=prompt("UPDATE ToDo",newEditItems);
      
      console.log(tmp1)
      
      let tmp2 =JSON.stringify(tmp1)
      localStorage.setItem("lists", tmp2)

      console.log(newEditItems)
      setTask(getLocalItem())

    }
  
    useEffect(()=>{
        localStorage.setItem("lists",JSON.stringify(task))
    },[task])

return( 
          <div className='container'>
                  
                  <h4>Enter ToDo</h4>
                  
                  <form onSubmit={submitHandler}>
                      <InputGroup className="mb-3">
                        <Form.Control id="todo-input" type="text" value={text} onChange={changeText} 
                        />
                        <Button type='submit' variant="outline-secondary" id="button-addon2">
                          Add To Do
                        </Button>
                      </InputGroup>
                  </form>
                  <h4>ToDo List</h4>
    
                  <Table striped bordered hover variant="dark">
                    <thead>
                      <tr>
                        <th>Task No</th>
                        <th>To Do</th>
                        <th>Delete</th>
                        <th>Update</th>
                      </tr>
                    </thead>
                    <tbody>
                    {
                          task.map((value,index) => {
                              
                              return (
                                  <>
                                      <tr>
                                        <td>{index+1}</td>
                                        <td><div key={index}>{value}</div></td>
                                        <td><div><button onClick={()=>removeTask(index)}>DELETE</button></div></td>
                                        <td><div><button onClick={()=>updateTask(index)}>UPDATE</button></div></td>
                                      </tr>
                                      
                                  </>
                              );
                          })
                      }
                    </tbody>
                  </Table>
    
            </div>      
)
}

export default App;
